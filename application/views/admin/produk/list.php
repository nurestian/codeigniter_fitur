
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Admin Dashboard
                            <small>statistics, charts, recent events and reports</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="portlet-body form">
                                        <form role="form" method="post" action="<?php echo base_url().'produk/tambah_produk'; ?>">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label>AUTOSELECT</label>
                                                    <select class="form-control cari" id="cari_id" name="cari">
                                                        <option value="">Pilih Opsi</option>
                                                        <?php foreach ($forms as $f) { ?>
                                                        <option value="<?php echo $f['id']; ?>" data-tanggal = "<?php echo $f['tanggal']; ?>"><?php echo $f['nama_baran']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <label>Input 1</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-plus"></i>
                                                        </span>
                                                        <input type="text" class="form-control" id="input1" placeholder="" readonly="true" value="" name="input1"> </div>
                                                </div> -->
                                                <div class="form-group">
                                                    <label>Tanggal</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-plus"></i>
                                                        </span>
                                                        <input type="text" class="form-control" id="tanggal" placeholder="" readonly="true" value="" name="tanggal"> </div>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <label>Input 2</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon input-circle-left">
                                                            <i class="fa fa-plus"></i>
                                                        </span>
                                                        <input type="text" class="form-control input-circle-right" name="input2" id="input2" placeholder=""> </div>
                                                </div>
                                                <div class="form-group">
                                                         <label>Total</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon input-circle-left">
                                                            <i class="fa-equals"></i>
                                                        </span>
                                                        <input type="text" class="form-control input-circle-right" name="total" id="total" placeholder="Total"> </div>
                                                </div> -->

                                                <div class="table-scrollable">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <td>Detail Barang</td>
                                                            <td>Harga Modal</td>
                                                            <td>Stok</td>
                                                            <td>Aksi</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="container"><!--Isian row--></tbody>
                                                </table>
                                            </div>

                                            </div>
                                            <div class="form-actions">
                                                <button type="submit" id="submit" class="btn blue">Submit</button>
                                            </div>
                                        </form></div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->


            