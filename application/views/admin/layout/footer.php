<!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2016 &copy; Metronic Theme By
                    <a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp;
                    <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- END QUICK NAV -->

        <!-- BEGIN CORE PLUGINS -->
        <script src="<?=base_url('assets/global/plugins/jquery.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/js.cookie.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/jquery.blockui.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=base_url('assets/global/plugins/moment.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/morris/morris.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/morris/raphael-min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/counterup/jquery.waypoints.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/counterup/jquery.counterup.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/fullcalendar/fullcalendar.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/horizontal-timeline/horizontal-timeline.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/flot/jquery.flot.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/flot/jquery.flot.resize.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/flot/jquery.flot.categories.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/jquery.sparkline.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/global/plugins/select2/js/select2.min.js');?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=base_url('assets/global/scripts/app.min.js');?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=base_url('assets/pages/scripts/dashboard.min.js');?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?=base_url('assets/layouts/layout/scripts/layout.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/layouts/layout/scripts/demo.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/layouts/global/scripts/quick-sidebar.min.js');?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/layouts/global/scripts/quick-nav.min.js');?>" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
     <script>
                $(document).ready(function(){

                    // $('#cari_id').on('change', function(){
                    //     var stok = $(this).find(':selected').data('stok');
                    //     var tanggal = $(this).find(':selected').data('tanggal');
                    //     $('#input1').val(stok);
                    //     $('#tanggal').val(tanggal);

                    //     console.log(stok);
                    // });


                    // $('#input2').on('keyup', function(){
                    //     var input1 = document.getElementById("input1").value;
                    //     var input2 = Number($(this).val());

                    //     var total = parseInt(input1) + parseInt(input2);

                    //     console.log(input2);
                    //     console.log(total);

                    //     $("#total").val(total);
                    // });


                    var count = 0;
                    var html = '';
                    $('#cari_id').on('change', function(){
                        $(".records").empty();
                        var id = $(this).val();
                        console.log(id);
                        var tanggal = $(this).find(':selected').data('tanggal');
                        $('#tanggal').val(tanggal);
                        count += 1;
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url().'admin/produk/get_list_barang'?>",
                            dataType: "json",
                            data : {id : id},
                            success: function(data) {
                                console.log(data);
                                $.each(data, function(i,o){
                                    html = `<tr class="records">`;
                                        
                                            html += `<td>
                                                        <input type="text" class="form-control" id="detail_barang" placeholder="" readonly="true" value="${o.detail_barang}" name="detail_barang">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" id="detail_barang" placeholder="" readonly="true" value="${o.harga_modal}" name="detail_barang">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" id="detail_barang" placeholder="" readonly="true" value="${o.stok}" name="detail_barang">
                                                    </td>`;
                                        
                                    html += `<td><a class="remove_item" ><button class="btn btn-danger"><i class="fa fa-trash-o"></i></button></a><input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td></tr>`;

                                $('#container').append(html);
                                });

                            }

                        });     
                    }); 


                });


            </script>

</html>