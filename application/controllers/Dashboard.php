<?php 
	defined('BASEPATH') OR exit ('No direct script access allowed');

	/**
	 * 
	 */
	class Dashboard extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();

		}
		public function index(){

			$data = array(
					'judul' => 'Halaman Dashboard',
					'isi'   => 'admin/dashboard/list'
					);

			$this->load->view('admin/layout/wrapper', $data, FALSE);
		}
	}
		

 ?>